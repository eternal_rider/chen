package chen.repository;

import chen.ChenException;

public class RepositoryFactory {
	private static DataBase db = DataBase.init();
	public static IRepository create(){
		String type = db.DbType();
		if(type.equals("oracle.jdbc.driver.OracleDriver")){
			return new OracleRepository(db);
		}else if(type.equals("com.mysql.jdbc.Driver")){
			return new MysqlRepository(db);
		}else if(type.equals("org.h2.Driver")){
			return new H2Repository(db);
		}else{
			throw new ChenException(" >>> chen 不支持的数据库类"+type,new Exception());
		}
	}
}