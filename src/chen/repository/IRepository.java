package chen.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;

public interface IRepository {
	
	public Map<String, Object> load(Object o, Map<String, Object> params) throws SQLException;
	public Map<String, Object> load(String sql, Object[] params) throws SQLException;
	public Map<String, Object> load(String sql,  Map<String,Object> params) throws SQLException;

	public List<Map<String, Object>> query(String sql, Object[] params) throws SQLException;
	public List<Map<String, Object>> query(String sql, Map<String,Object> params) throws SQLException;
	public List<Map<String, Object>> query(String sql) throws SQLException;

	public int save(Object o, Map<String,Object> params) throws SQLException;
	
	public int update(String sql, Object[] params) throws SQLException;
	public int update(String sql, Map<String,Object> params) throws SQLException;
	public int update(Object o, Map<String,Object> params,String where) throws SQLException;
	
	public int delete(Object o, Map<String,Object> params,String where) throws SQLException;
	
	public void setParams(PreparedStatement stmt, Object... params) throws SQLException;

	public void close(Statement stmt) throws SQLException;

	public void close(ResultSet rs) throws SQLException;
}