package chen.repository;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Vector;

import chen.ChenException;

public class DataBase {
	private Vector<Connection> pool;
	private static DataBase instance = null;
	private String type;
	private int init_active = 10;
	private int curr_active = 10;
	private int max_active = 50;
	
	private DataBase(){
		this.pool = new Vector<Connection>();
		InputStream propStream = DataBase.class.getResourceAsStream("/database.properties");
		if (propStream != null) {
			try {
				Properties props = new Properties();
				props.load(propStream);
				init_active = Integer.parseInt(props.getProperty("initial.active"));
				max_active = Integer.parseInt(props.getProperty("max.active"));
				String driver = props.getProperty("jdbc.driver");
				Class.forName(driver);
				type = driver;
				for(int c=0;c<max_active;c++){
					Connection conn = DriverManager.getConnection(props.getProperty("jdbc.url"),props.getProperty("jdbc.username"),props.getProperty("jdbc.password"));
					pool.add(conn);
				}
			} catch (Exception e) {
				this.closePool();
				this.pool = null;
				throw new ChenException(" >>> chen 初始化数据库时失败",e);
			} finally {
				try {
					propStream.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	public synchronized void release(Connection conn){
		pool.add(conn);
	}
	public synchronized void closePool(){
		for(int c=0;c<pool.size();c++){
			try {
				pool.get(c).close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			pool.remove(c);
		}
	}

	public synchronized Connection getConnection(){
		if(pool.size()>0){
			Connection conn = pool.get(0);
			pool.remove(conn);
			return conn;
		}else{
			return null;
		}
	}
	
	public String DbType(){
		return this.type;
	}
	public static DataBase init(){
		if(instance == null){
			instance = new DataBase();
		}
		return instance;
	}
	public static void destroy(){
		DataBase.instance.closePool();
	}
}