package chen.repository;

import java.sql.Connection;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OracleRepository implements IRepository {
	private DataBase instance;
	public OracleRepository(DataBase instance){
		this.instance = instance;
	}
	
	public Map<String, Object> load(Object o, Map<String, Object> params)
			throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;

		try {
			stmt = this.prepareStatement(conn, this.modelConverterSelectSql(o, params));
			rs = stmt.executeQuery();
			return this.mapConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
	}
	
	public Map<String, Object> load(String sql, Object[] params)
			throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<String, Object> result = null;

		try {
			stmt = this.prepareStatement(conn, sql);
			if (params != null) {
				this.setParams(stmt, params);
			}
			rs = stmt.executeQuery();
			result = this.mapConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return result;
	}

	@Override
	public Map<String, Object> load(String sql, Map<String, Object> params)	throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<String, Object> result = null;

		try {
			if (params != null) {
				sql = this.setParams(sql, params);
			}
			stmt = this.prepareStatement(conn, sql);
			rs = stmt.executeQuery();
			result = this.mapConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> query(String sql) throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Map<String, Object>> result = null;

		try {
			stmt = this.prepareStatement(conn, sql);
			rs = stmt.executeQuery();
			result = this.listConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return result;
	}
	
	@Override
	public List<Map<String, Object>> query(String sql,Map<String, Object> params) throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Map<String, Object>> result = null;

		try {
			if (params != null) {
				sql = this.setParams(sql, params);
			}
			stmt = this.prepareStatement(conn, sql);
			rs = stmt.executeQuery();
			result = this.listConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return result;
	}

	public List<Map<String, Object>> query(String sql, Object[] params)
			throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Map<String, Object>> result = null;

		try {
			stmt = this.prepareStatement(conn, sql);
			if (params != null) {
				this.setParams(stmt, params);
			}
			rs = stmt.executeQuery();
			result = this.listConverter(rs);
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return result;
	}

	public int update(String sql, Object[] params) throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			stmt = this.prepareStatement(conn, sql);
			this.setParams(stmt, params);
			rows = stmt.executeUpdate();
		} finally {
			close(stmt);
			instance.release(conn);
		}

		return rows;
	}

	public int update(String sql,Map<String, Object> params) throws SQLException {
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			stmt = this.prepareStatement(conn, sql);
			if (params != null) {
				this.setParams(stmt, params);
			}
			rows = stmt.executeUpdate();
		} finally {
			close(stmt);
			instance.release(conn);
		}

		return rows;
	}
	
	public int update(Object o, Map<String,Object> params,String where) throws SQLException{
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			String sql = this.modelConverterUpdateSql(o, params);
			if(where != null){
				sql += " where "+this.setParams(where, params);
			}
			stmt = this.prepareStatement(conn, sql);
			rows = stmt.executeUpdate();
		} finally {
			close(stmt);
			instance.release(conn);
		}

		return rows;
	}
	public int save(Object o, Map<String,Object> params) throws SQLException{
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			String sql = this.modelConverterInsertSql(o, params);
			stmt = this.prepareStatement(conn, sql);
			rows = stmt.executeUpdate();
		} finally {
			close(stmt);
			instance.release(conn);
		}

		return rows;
	}
	
	public int delete(Object o, Map<String,Object> params,String where) throws SQLException{
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		int rows = 0;
		try {
			String sql = this.modelConverterDeleteSql(o);
			if(where != null){
				sql += " where "+this.setParams(where, params);
			}
			stmt = this.prepareStatement(conn, sql);
			rows = stmt.executeUpdate();
		} finally {
			close(stmt);
			instance.release(conn);
		}
		return rows;
	}
	
	public void setParams(PreparedStatement stmt, Object... params)
			throws SQLException {
		if (params == null) {
			return;
		}

		ParameterMetaData pmd = null;
		pmd = stmt.getParameterMetaData();
		if (pmd.getParameterCount() < params.length) {
			throw new SQLException("Too many parameters: expected "
					+ pmd.getParameterCount() + ", was given " + params.length);
		}
		for (int i = 0; i < params.length; i++) {
			if (params[i] != null) {
				stmt.setObject(i + 1, params[i]);
			} else {
				int sqlType = Types.VARCHAR;
				try {
					sqlType = pmd.getParameterType(i + 1);
				} catch (SQLException e) {
				}
				stmt.setNull(i + 1, sqlType);
			}
		}
	}
	
	public String setParams(String sql, Map<String,Object> params){
		for(String key : params.keySet()){
			Object v = params.get(key);
			if(v instanceof String){
				sql.replaceAll("#"+key+"#", "'"+v.toString()+"'");
			}else{
				sql.replaceAll("#"+key+"#", String.valueOf(v));
			}
		}
		
		return sql;
	}

	private Map<String, Object> mapConverter(ResultSet rs) throws SQLException {
		Map<String, Object> result = null;
		if (rs.next()) {
			result = new HashMap<String, Object>();
			ResultSetMetaData metaData = rs.getMetaData();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				String filed = metaData.getColumnName(i).toLowerCase();
				result.put(filed, rs.getObject(filed));
			}
		}
		return result;
	}

	private List<Map<String, Object>> listConverter(ResultSet rs)
			throws SQLException {
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
		ResultSetMetaData metaData = rs.getMetaData();
		while (rs.next()) {
			Map<String, Object> rowData = new HashMap<String, Object>();
			for (int i = 1; i <= metaData.getColumnCount(); i++) {
				String filed = metaData.getColumnName(i).toLowerCase();
				rowData.put(filed, rs.getObject(filed));
			}
			result.add(rowData);
		}
		if (result.size() > 0) {
			return result;
		} else {
			return null;
		}

	}

	private PreparedStatement prepareStatement(Connection conn, String sql)
			throws SQLException {
		return conn.prepareStatement(sql);
	}

	public void close(Statement stmt) throws SQLException {
		if (stmt != null) {
			stmt.close();
		}
	}

	public void close(ResultSet rs) throws SQLException {
		if (rs != null) {
			rs.close();
		}
	}
	private String modelConverterSelectSql(Object o,Map<String, Object> params){
		String table = o.getClass().getSimpleName().toUpperCase().replace("MODEL", "");
		StringBuffer sql = new StringBuffer();
		sql.append("select * from "+table+" where 1=1 ");
		if(params != null){
			for(String key : params.keySet()){
				Object v = params.get(key);
				if(v instanceof String){
					sql.append(" and " + key +"='"+v+"'");
				}else{
					sql.append(" and " + key +"="+ v);
				}
			}
		}
		return sql.toString();
	}

	private String modelConverterUpdateSql(Object o,Map<String, Object> params) throws SQLException{
		String table = o.getClass().getSimpleName().toUpperCase().replace("MODEL", "");
		Map<String,String> meta = this.metadata("select * from "+table+" where 1=0");
		StringBuffer sql = new StringBuffer();
		
		if(params != null){
			sql.append("update "+table+" set ");
			for(String key : meta.keySet()){
				if(params.get(key) != null){
					String t = meta.get(key);
					if(t.toLowerCase().equals("varchar")){
						sql.append(key +"='"+params.get(key)+"',");
					}else{
						sql.append(key +"="+params.get(key)+",");
					}
				}
			}
		}else{
			 throw new SQLException(" >>> chen 对象转sql失败，参数为空");
		}
		return sql.deleteCharAt(sql.length()-1).toString();
	}

	private String modelConverterInsertSql(Object o,Map<String, Object> params) throws SQLException{
		String table = o.getClass().getSimpleName().toUpperCase().replace("MODEL", "");
		Map<String,String> meta = this.metadata("select * from "+table+" where 1=0");
		StringBuffer sql = new StringBuffer();
		StringBuffer vs = new StringBuffer();
		if(params != null){
			sql.append("insert into "+table+"(");
			vs.append(" values(");
			for(String key : meta.keySet()){
				if(params.get(key) != null){
					sql.append(key+",");
					String t = meta.get(key);
					if(t.toLowerCase().equals("varchar")){
						vs.append("'"+params.get(key)+"',");
					}else{
						vs.append(params.get(key)+",");
					}
				}
			}
		}else{
			 throw new SQLException(" >>> chen 对象转sql失败，参数为空");
		}
		sql.deleteCharAt(sql.length()-1);
		sql.append(")");
		vs.deleteCharAt(vs.length()-1);
		vs.append(")");
		return sql.toString()+vs.toString();
	}
	
	private String modelConverterDeleteSql(Object o){
		String table = o.getClass().getSimpleName().toUpperCase().replace("MODEL", "");
		StringBuffer sql = new StringBuffer();
		sql.append("delete from "+table);
		return sql.toString();
	}
	
	private Map<String,String> metadata(String table) throws SQLException{
		String sql = "select COLUMN_NAME,DATA_TYPE from user_tab_columns where Table_Name='"+table+"'";
		Connection conn = instance.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Map<String,String> meta = null;
		try {
			stmt = this.prepareStatement(conn, sql);
			rs = stmt.executeQuery();
			meta = new HashMap<String,String>();
			while(rs.next()){
				meta.put(rs.getString("COLUMN_NAME").toLowerCase(),rs.getString("DATA_TYPE").toLowerCase());
			}
		} finally {
			try {
				close(rs);
			} finally {
				close(stmt);
			}
			instance.release(conn);
		}
		return meta;
	}
}