package chen;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import chen.aop.Binder;
import chen.aop.ChenAop;
import chen.aop.GZIPResponseWrapper;
import chen.util.ContextUtil;

@WebFilter(filterName="filter1",urlPatterns = { "/*" })
public class FrontControl1 implements Filter{
	
	private AtomicBoolean initialized = new AtomicBoolean();
	private ServletContext servletContext;
	
	@Override
    public void init(final FilterConfig config) throws ServletException{
        if (initialized.compareAndSet(false, true)) {
        	this.servletContext = config.getServletContext();
        }
	}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws ServletException, IOException{
    	HttpServletRequest req = (HttpServletRequest) request;
    	HttpServletResponse res = (HttpServletResponse) response;
    	ContextUtil.setActionContext(this.servletContext,req, res);
    	
    	String encoding = App.get("Encoding");
    	if(encoding != null && encoding.length()>0){
    		//转码
    		if (request.getCharacterEncoding() == null) {
    			request.setCharacterEncoding(encoding);
    		}
    	}
    	
    	//全局aop只执行before，这个还得考虑下
    	try {
        	if(this.chenAopValid(req)){
        		ChenAop chenAop = new ChenAop(req);
        		chenAop.before(null);
        		//chenAop.after(null);
        		chenAop = null;
        	}
        }
        catch (Exception e) {
        	throw new ChenException(" >>> chen 执行全局Aop时失败",e);
        }

    	//压缩
    	String compress = App.get("Compress");
    	if(compress != null && compress.equals("true")){
    		if (request instanceof HttpServletRequest) {
    			String ae = req.getHeader("accept-encoding");
    			if (ae != null && ae.indexOf("gzip") != -1) {
    				GZIPResponseWrapper wrappedResponse = new GZIPResponseWrapper(res);
    				try {
    					chain.doFilter(req, wrappedResponse);
    				} catch (Exception e) {
    					throw new ChenException(" >>> chen 执行压缩时失败",e);
    				} finally {
    					wrappedResponse.finishResponse();
    				}
    				return;
    			} else {
    				chain.doFilter(req, res);
    			}
    		}
    	}else{
    		chain.doFilter(req, res);
    	}

    }
	private boolean chenAopValid(HttpServletRequest req){
        String uri = req.getRequestURI();
        String prefix = App.get("ActionPrefix");
		String urls = App.get("GlobelAopImmuneUrl");
		String suffixs = App.get("GlobelAopSuffix");
		
		int n = uri.indexOf("?");
		if(n != -1){
			uri = uri.substring(0,n);
		}
		
		if(urls != null){
			for(String url : urls.split(",")){
				if(uri.indexOf(url) != -1){
					return false;
				}
			}
		}
		
		boolean flag = false;
		if(suffixs != null){
			for(String suffix : suffixs.split(",")){
				if(uri.endsWith(suffix)){
					flag = true;
					break;
				}
			}
		}
		if(flag || uri.indexOf(prefix) != -1){
			flag = true;
		}

		String[] aop = Binder.getAop("/*");
    	if(aop != null && aop.length>0 && flag){
    		return true;
    	}else{
    		return false;
    	}
	}
    @Override
    public void destroy() {}
}