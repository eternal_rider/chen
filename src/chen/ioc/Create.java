package chen.ioc;

import chen.ChenException;

import com.googlecode.totallylazy.*;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import static com.googlecode.totallylazy.Callables.cast;
import static com.googlecode.totallylazy.Callables.descending;
import static com.googlecode.totallylazy.Option.none;
import static com.googlecode.totallylazy.Option.some;
import static com.googlecode.totallylazy.Sequences.sequence;

public class Create<T> implements Callable<T> {
    private final Class<T> concrete;
    private final Resolver resolver;

    private Create(Class<T> concrete, final Resolver resolver) {
        this.concrete = concrete;
        this.resolver = resolver;
    }

    public static <T> Create<T> create(Class<T> concrete, final Resolver resolver) {
        return new Create<T>(concrete, resolver);
    }

    public T call() throws Exception {
        Sequence<Constructor<?>> constructors = sequence(concrete.getConstructors()).sortBy(descending(numberOfParamters()));
        if (constructors.isEmpty()) {
            throw new ChenException(concrete.getName() + " does not have a public constructor");
        }
        final List<ChenException> exceptions = new ArrayList<ChenException>();
        return constructors.tryPick(firstSatisifiableConstructor(exceptions)).map(cast(concrete)).
                getOrElse(Callables.<T>callThrows(new ChenException(concrete.getName() + " does not have a satisfiable constructor", exceptions)));
    }

    private Callable1<Constructor<?>, Option<Object>> firstSatisifiableConstructor(final List<ChenException> exceptions) {
        return new Callable1<Constructor<?>, Option<Object>>() {
            public Option<Object> call(Constructor<?> constructor) throws Exception {
                try {
                    Sequence<Object> instances = sequence(constructor.getParameterTypes()).map(convertToCallable(resolver));
                    return (Option<Object>) some(constructor.newInstance(instances.toArray(Object.class)));
                } catch (ChenException e) {
                    exceptions.add(e);
                    return none();
                }
            }
        };
    }

    private Callable1<Constructor<?>, Comparable> numberOfParamters() {
        return new Callable1<Constructor<?>, Comparable>() {
            public Comparable call(Constructor<?> constructor) throws Exception {
                return constructor.getParameterTypes().length;
            }
        };
    }

    private Callable1<? super Class<?>, Object> convertToCallable(final Resolver resolver) {
        return new Callable1<Class<?>, Object>() {
            public Object call(Class<?> aClass) throws Exception {
                return resolver.resolve(aClass);
            }
        };
    }
    
    /////////////////////////
    public static <T> T call(Callable<T> callable) {
        try {
            return callable.call();
        } catch (Exception e) {
            throw new ChenException(" >>> chen Ioc��������",e);
        }
    }
    public static <T> Callable<T> returns(final T t) {
        return new Callable<T>() {
            public T call() throws Exception {
                return t;
            }
        };
    }
}
