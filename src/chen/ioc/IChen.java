package chen.ioc;

import java.util.concurrent.Callable;

public interface IChen extends Resolver {
    <C> IChen add(Class<C> concrete);

    <I, C extends I> IChen add(Class<I> anInterface, Class<C> concrete);

    <T> IChen addInstance(T instance);

    <I, C extends I> IChen addInstance(Class<I> anInterface, C instance);

    <T, A extends Callable<T>> IChen addActivator(Class<T> aClass, Class<A> activator);

    <T> IChen addActivator(Class<T> aClass, Callable<? extends T> activator);

    <I, C extends I> IChen decorate(Class<I> anInterface, Class<C> concrete);

    <T> Callable<T> remove(Class<T> aClass);

    <T> boolean contains(Class<T> aClass);

    <T> T get(Class<T> aClass);

    <T> Callable<T> getActivator(Class<T> aClass);
}
