package chen.ioc;

import static chen.ioc.Create.create;
import static chen.ioc.Create.returns;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import chen.ChenException;

public class Chen implements IChen {
	public static final Chen container = new Chen();
    private final Map<Class, Callable> activators = new HashMap<Class, Callable>();
    private final Resolver missingHandler;

    public Chen(Resolver missingHandler) {
        this.missingHandler = missingHandler;
    }

    public Chen() {
        this(new Resolver() {
            public Object resolve(Class aClass) {
                throw new ChenException(" >>> chen 在容器中未找到"+aClass.getName() + "类");
            }
        });
    }

    public Object resolve(Class aClass) {
        if (!activators.containsKey(aClass)) {
            return missingHandler.resolve(aClass);
        }
        return Create.call(activators.get(aClass));
    }

    public <T> T get(Class<T> aClass) {
        return (T) resolve(aClass);
    }

    public <T> Callable<T> getActivator(Class<T> aClass) {
        return activators.get(aClass);
    }

    public <T> IChen add(final Class<T> concrete) {
        return addActivator(concrete, create(concrete, this));
    }

    public <I, C extends I> IChen add(Class<I> anInterface, Class<C> concrete) {
        return addActivator(anInterface, create(concrete, this));
    }

    public <T> IChen addInstance(T instance) {
        return addActivator((Class<T>)instance.getClass(), returns(instance));
    }

    public <I, C extends I> IChen addInstance(Class<I> anInterface, C instance) {
        return addActivator(anInterface, returns(instance));
    }

    public <T, A extends Callable<T>> IChen addActivator(Class<T> aClass, final Class<A> activator) {
        return add(activator).addActivator(aClass, new Callable<T>() {
            public T call() throws Exception {
                return get(activator).call();
            }
        });
    }

    public <T> IChen addActivator(Class<T> aClass, Callable<? extends T> activator) {
        if (activators.containsKey(aClass)) {
            throw new ChenException(" >>> chen 类"+aClass.getName() + "已经在Ioc容器中存在");
        }
        //activators.put(aClass, lazy(activator));
        activators.put(aClass, activator);
        return this;
    }

    public <I, C extends I> IChen decorate(final Class<I> anInterface, final Class<C> concrete) {
        final Callable existing = activators.get(anInterface);
        //activators.put(anInterface, lazy(create(concrete, new Resolver() {
        activators.put(anInterface, create(concrete, new Resolver() {
                    public Object resolve(Class aClass) {
                        if (aClass.equals(anInterface)) return Create.call(existing);
                        return get(aClass);
                    }
                }));
        return this;
    }

    public <T> Callable<T> remove(Class<T> aClass) {
        return activators.remove(aClass);
    }

    public <T> boolean contains(Class<T> aClass) {
        return activators.containsKey(aClass);
    }
}