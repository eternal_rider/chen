package chen.ioc;

public interface Resolver {
    Object resolve(Class aClass);
}
