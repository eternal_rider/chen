package chen;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import chen.ioc.Chen;
public class Scaner {
    private static Map<String,String> clsMap = new HashMap<String,String>();
    public static void run() throws ClassNotFoundException, InstantiationException, IllegalAccessException{
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		URL resource = loader.getResource("/");
		File dirs = new File(resource.getFile());
		scan(dirs,"",loader);
		System.out.println(">>> chen Ioc容器添加Action和Model共"+clsMap.size()+"个");
    }

	private static void scan(File dirs,String basePack,ClassLoader loader)
	throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		File[] childs = dirs.listFiles();
		for (int i = 0; i < childs.length; i++) {
			String packPath =basePack+childs[i].getName()+".";
			if (childs[i].isDirectory()) {
				scan(childs[i],packPath,loader);
			} else {
				String className = childs[i].getName();
				if (className.endsWith("Action.class") || className.endsWith("Model.class")) {
					packPath=packPath.replace(".class.", "");
					String clsName = packPath.substring(packPath.lastIndexOf(".")+1);
					if(clsMap.get(clsName) != null){
						new IllegalAccessException(clsName+" class 重复");
					}else{
						clsMap.put(clsName.toLowerCase(), packPath);
						Chen.container.add(Class.forName(packPath, false, loader));
					}
				}
			}
		}
	}
	public static Map<String,String> getCls(){
		return clsMap;
	}
}