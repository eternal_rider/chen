package chen.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JspAsyncRenderer extends Renderer {

    private String path;
    private Map<String, Object> model;
    protected String contentType;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    public JspAsyncRenderer(String path) {
        this.path = path;
        this.model = new HashMap<String, Object>();
    }

    public JspAsyncRenderer(String path, Map<String, Object> model) {
        this.path = path;
        this.model = model;
    }

    public JspAsyncRenderer(String path, String modelKey, Object modelValue) {
        this.path = path;
        this.model = new HashMap<String, Object>();
        this.model.put(modelKey, modelValue);
    }
    @Override
    public void render(ServletContext context, HttpServletRequest request, HttpServletResponse response,AsyncContext async) throws Exception {
        Set<String> keys = model.keySet();
        for (String key : keys) {
        	async.getRequest().setAttribute(key, model.get(key));
        }
        if(path != null && path.length() > 0){
        	async.dispatch(path);
        }else{
        	async.dispatch();
        }
    }

}