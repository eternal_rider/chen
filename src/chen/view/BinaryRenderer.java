package chen.view;

import java.io.OutputStream;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BinaryRenderer extends Renderer {

    private byte[] data;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public void render(ServletContext context, HttpServletRequest request, HttpServletResponse response,AsyncContext async) throws Exception {
        response.setContentType(contentType==null ? "application/octet-stream" : contentType);
        response.setContentLength(data.length);
        OutputStream output = response.getOutputStream();
        output.write(data);
        output.flush();
    }
}
