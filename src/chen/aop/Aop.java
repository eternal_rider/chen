package chen.aop;

import java.util.Map;

public interface Aop {
    void before(Map<String,Object> args);
    void after(Map<String,Object> args);
    //void before(TinyMap args);
    //void after(TinyMap args);
}
