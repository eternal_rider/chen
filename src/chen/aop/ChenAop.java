package chen.aop;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import chen.ChenException;
import chen.ioc.Chen;
import chen.util.ConverterUtil;

public class ChenAop implements Aop {
	private Object[] aopIns;
	private Map<String,Object> args;
	
	public ChenAop(HttpServletRequest req){
    	String[] aop = Binder.getAop("/*");
    	if(aop != null && aop.length>0){
    		this.args = ConverterUtil.arr2ObjForMap(req.getParameterMap());
    		int n = aop.length;
    		this.aopIns = new Object[n];
	    	for(int i = 0;i<n;i++){
	    		try {
					this.aopIns[i] = Chen.container.get(Class.forName(aop[i]));
				} catch (Exception e) {
					 throw new ChenException(" >>> chen 加载全局Aop时失败",e);
				}
	    	}
    	}
	}
	@Override
	public void before(Map<String,Object> args){
		for(int i = 0;i<this.aopIns.length;i++){
			try {
				this.aopIns[i].getClass().getMethod("before",Map.class).invoke(this.aopIns[i],this.args);
			} catch (Exception e) {
				throw new ChenException(" >>> chen 执行全局Aop的"+this.aopIns[i].getClass().getName()+".before方法时失败",e);
			}
		}
	}

	@Override
	public void after(Map<String,Object> args){
		if(this.aopIns != null && this.aopIns.length>0){
	    	for(int i = 0;i<this.aopIns.length;i++){
    			try {
					this.aopIns[i].getClass().getMethod("after",Map.class).invoke(this.aopIns[i], this.args);
				} catch (Exception e) {
					throw new ChenException(" >>> chen 执行全局Aop的"+this.aopIns[i].getClass().getName()+".after方法时失败",e);
				}
	    	}
		}
		this.aopIns = null;
	}
}
