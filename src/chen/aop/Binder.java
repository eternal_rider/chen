package chen.aop;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.catalina.Loader;

import chen.App;
import chen.ioc.Chen;
import chen.repository.DataBase;

public final class Binder {
	private static Map<String,String[]> aops =new HashMap<String,String[]>();
	public static void load() throws IOException, ClassNotFoundException{
		InputStream propStream = DataBase.class.getResourceAsStream("/aops.properties");
		Properties props = new Properties();
		
		int n = 0;
		if (propStream != null) {
			try {
				props.load(propStream);
				ClassLoader loader = Thread.currentThread().getContextClassLoader();
				for(Object key : props.keySet()){
					String[] tmp = props.get(key).toString().split(",");
					aops.put(key.toString(),tmp);
					n += tmp.length;
					for(String aop : tmp){
						Chen.container.add(Class.forName(aop,false,loader));
					}
				}
				loader = null;
			} finally {
					propStream.close();
					props.clear();
					System.out.println(">>> chen Ioc��������Aop��"+n+"��");
			}
		}
	}
	public static String[] getAop(String[] route){
		String[] reqAop = null;
		String[] aops1 = aops.get("/"+route[0]+"/"+route[1]+"/");
		String[] aops2 = aops.get("/"+route[0]+"/*");
		//String[] aops3 = aops.get("/*");
    	int aopNum1 = aops1 !=null?aops1.length:0;
    	int aopNum2 = aops2 != null?aops2.length:0;
    	//int aopNum3 = aops3 !=null?aops3.length:0;
    	//if(aopNum3+aopNum2+aopNum1 !=0){
    	if(aopNum2+aopNum1 !=0){
    		//reqAop = new String[aopNum3+aopNum2+aopNum1];
    		reqAop = new String[aopNum2+aopNum1];

/*    		if(aopNum3>0){
    			System.arraycopy(aops3,0,reqAop,0,aops3.length);
    		}
    		if(aopNum2>0){
    			System.arraycopy(aops2,0,reqAop,aopNum3,aops2.length);
    		}
    		if(aopNum1>0){
    			System.arraycopy(aops1,0,reqAop,aopNum2+aopNum3,aops1.length);
    		}*/
    		if(aopNum2>0){
    			System.arraycopy(aops2,0,reqAop,0,aops2.length);
    		}
    		if(aopNum1>0){
    			System.arraycopy(aops1,0,reqAop,aopNum2,aops1.length);
    		}
    	}
    	return reqAop;
	}
	public static String[] getAop(String route){
		if(route == null){
			return null;
		}
    	return aops.get(route);
	}
}
