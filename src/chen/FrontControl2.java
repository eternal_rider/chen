package chen;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.AsyncContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chen.aop.Binder;
import chen.handler.AsynHandler;
import chen.handler.ChenHandler;
import chen.handler.Context;
import chen.handler.IHandler;
import chen.repository.DataBase;
import chen.repository.RepositoryFactory;

@WebFilter(filterName="filter2",urlPatterns = { "/*" }, asyncSupported = true)
public class FrontControl2 implements Filter{
	
	private AtomicBoolean initialized = new AtomicBoolean();
	private ServletContext servletContext;

	@Override
    public void init(final FilterConfig config) throws ServletException{
        try {
            if (initialized.compareAndSet(false, true)) {
            	long time1 = System.currentTimeMillis();
            	this.servletContext = config.getServletContext();
                Scaner.run();
                Binder.load();
                App.load();
                DataBase.init();
                System.out.println(">>> chen 已经启动，用时"+(System.currentTimeMillis()-time1)/1000+"秒");
            }
        }
        catch (Exception e) {
            throw new ChenException(" >>> chen 启动失败",e);
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws ServletException, IOException{
    	HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        
    	try {
	        String[] routes = this.valid(req);
	        if(routes == null){
	        	chain.doFilter(request, response);
	        	return;
	        }
        
        	long time1 = System.currentTimeMillis();
            Context context = null;
            IHandler handler = null;
            
            //异步请求
            if(routes[0].endsWith("async")){
            	AsyncContext async = req.startAsync();  
		        async.setTimeout(30*1000);
		        context = new Context(routes,Scaner.getCls(),Binder.getAop(routes),req,res,servletContext,async);
		        async.start(new AsynHandler(context));
            }else{
            	context = new Context(routes,Scaner.getCls(),Binder.getAop(routes),req,res,servletContext);
            	handler = new ChenHandler(context);
            	handler.init();
            }
            System.out.println(">>> chen 处理路由/"+routes[0]+"/"+routes[1]+"/完成，用时"+(System.currentTimeMillis()-time1)/1000+"秒");
        }
        catch (Exception e) {
        	throw new ChenException(" >>> chen 处理地址："+req.getRequestURI()+" 时失败",e);
        }
    }
    private String[] valid(HttpServletRequest req){
        String uri = req.getRequestURI();
        String path = req.getContextPath();
        String prefix = App.get("ActionPrefix");
        if(uri.indexOf(prefix) == -1){
        	return null;
        }
        if (uri != null && uri.length()>path.length()){
        	uri = uri.substring(path.length());
        }else{
        	return null;
        }
        String[] routes = uri.substring(uri.indexOf(prefix)+prefix.length()).split("/");
        if(routes == null || routes.length<2){
        	return null;
        }
        return routes;
    }
	private boolean chenAopValid(String uri,String prefix){
		String urls = App.get("GlobelAopImmuneUrl");
		String suffixs = App.get("GlobelAopSuffix");
		
		int n = uri.indexOf("?");
		if(n != -1){
			uri = uri.substring(0,n);
		}
		
		if(urls != null){
			for(String url : urls.split(",")){
				if(uri.indexOf(url) != -1){
					return false;
				}
			}
		}
		
		boolean flag = false;
		if(suffixs != null){
			for(String suffix : suffixs.split(",")){
				if(uri.endsWith(suffix)){
					flag = true;
					break;
				}
			}
		}
		if(flag || uri.indexOf(prefix) != -1){
			flag = true;
		}

		String[] aop = Binder.getAop("/*");
    	if(aop != null && aop.length>0 && flag){
    		return true;
    	}else{
    		return false;
    	}
	}
    @Override
    public void destroy() {
    	DataBase.destroy();
    }
}
