package chen;

import java.util.ArrayList;
import java.util.List;

public class ChenException extends RuntimeException{
    private List<ChenException> causes;

    public ChenException(String message) {
        this(message, new ArrayList<ChenException>());
    }
    
    public ChenException(String message, Exception e) {
        super(message, e);
    }
    public ChenException(String message, List<ChenException> causes) {
        super(message, lastOrNull(causes));
        this.causes = causes;
    }

    private static ChenException lastOrNull(List<ChenException> causes) {
        if(causes.isEmpty()) return null;
        return causes.get(causes.size() - 1);
    }

    public List<ChenException> getCauses() {
        return causes;
    }
}