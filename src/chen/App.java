package chen;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import chen.repository.DataBase;
import chen.util.ContextUtil;

public final class App {
	private static Properties props;
	public static void load() throws IOException, ClassNotFoundException{
		InputStream propStream = DataBase.class.getResourceAsStream("/app.properties");
		props = new Properties();
		
		if (propStream != null) {
			try {
				props.load(propStream);
			} finally {
				propStream.close();
			}
		}
	}
	public static String get(String attr){
		return (String)props.get(attr);
	}
	public static String base(){
		HttpServletRequest req = ContextUtil.getContext().getHttpServletRequest();
		return req.getScheme()+"://"+req.getServerName()+":"+req.getServerPort()+req.getContextPath()+"/";
	}
}
