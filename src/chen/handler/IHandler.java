package chen.handler;


public interface IHandler {
	void init();
	void execute();
	void renderer(Object result);
	void destroy();
}