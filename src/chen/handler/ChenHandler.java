package chen.handler;

import java.lang.reflect.Method;
import java.util.Map;

import chen.App;
import chen.ChenException;
import chen.util.UploadUtil;
import chen.view.Renderer;

public class ChenHandler implements IHandler {

	private Context context;
	public ChenHandler(Context context){
		this.context = context;
	}
	public void init() {
    	if ("post".equals(this.context.reqs.getMethod().toLowerCase()) && this.context.reqs.getContentType() != null && this.context.reqs.getContentType().toLowerCase().startsWith("multipart/")){
    		UploadUtil upUtil = new UploadUtil(App.get("UploadFileDir"));
    		try {
    			Map<String,String> files = upUtil.upload();
    			this.context.args.putAll(files);
			} catch (Exception e) {
				throw new ChenException(" >>> chen 上传文件过程中出错",e);
			}
    	}
		this.execute();
	}

	public void execute() {
		Object instance = this.context.instance;
		Method method = this.context.method;
		Map<String,Object> args = this.context.args;

		try {
			if(this.context.aops != null && this.context.aops.length>0){
				for(Object aop : this.context.aops){
					aop.getClass().getMethod("before",Map.class).invoke(aop, args);
				}
			}
			Object result = method.invoke(instance, args);
			if(this.context.aops != null && this.context.aops.length>0){
				for(Object aop : this.context.aops){
					aop.getClass().getMethod("after",Map.class).invoke(aop, args);
				}
			}
			this.renderer(result);
		} catch (Exception e) {
			throw new ChenException(" >>> chen 调用"+instance.getClass().getName()+"."+this.context.method+"方法过程中出错",e);
		}
	}

	public void renderer(Object result) {
		try{
	        if (result==null){
	        	this.destroy();
	            return;
	        }
	        if (result instanceof Renderer) {
	            Renderer r = (Renderer) result;
	            r.render(this.context.servletContext, this.context.reqs, this.context.resp,this.context.async);
	            this.destroy();
	            return;
	        }
	        if (result instanceof String) {
	            String s = (String) result;
	            if (s.startsWith("/")) {
	                this.context.reqs.getRequestDispatcher(s).forward(this.context.reqs, this.context.resp);
	            }else{
	            	this.context.resp.getWriter().print(result);
	            }
	            this.destroy();
	        }
		} catch (Exception e) {
			throw new ChenException(" >>> chen 渲染"+this.context.instance.getClass().getName()+"."+this.context.method+" Action的视图过程中出错",e);
		}
	}
	@Override
	public void destroy() {
		this.context = null;
	}
}