package chen.util;

import javax.servlet.AsyncContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public final class ContextUtil {

    private static final ThreadLocal<ContextUtil> ContextThreadLocal = new ThreadLocal<ContextUtil>();

    private ServletContext context;
    private HttpServletRequest request;
    private HttpServletResponse response;
    private AsyncContext async;

    public AsyncContext getAsync() {
		return async;
	}

	public ServletContext getServletContext() {
        return context;
    }

    public HttpServletRequest getHttpServletRequest() {
        return request;
    }

    public HttpServletResponse getHttpServletResponse() {
        return response;
    }

    public HttpSession getHttpSession() {
        return request.getSession();
    }

    public static ContextUtil getContext() {
        return ContextThreadLocal.get();
    }

    public static void setActionContext(ServletContext context, HttpServletRequest request, HttpServletResponse response) {
    	ContextUtil ctx = new ContextUtil();
        ctx.context = context;
        ctx.request = request;
        ctx.response = response;
        ContextThreadLocal.set(ctx);
    }
    public static void setActionContext(ServletContext context, HttpServletRequest request, HttpServletResponse response,AsyncContext async) {
    	ContextUtil ctx = new ContextUtil();
        ctx.context = context;
        ctx.request = request;
        ctx.response = response;
        ctx.async = async;
        ContextThreadLocal.set(ctx);
    }

    public static void removeActionContext() {
    	ContextThreadLocal.remove();
    }
}
