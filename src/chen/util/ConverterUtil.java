package chen.util;

import java.util.Arrays;
import java.util.Map;

public class ConverterUtil {
    public static Map<String,Object> arr2ObjForMap(Map<String,String[]> args){
    	if(args == null){
    		return null;
    	}
        Map<String, Object> params = new ChenMap();
        for(String key : args.keySet()){
        	params.put(key, Arrays.toString(args.get(key)).replaceAll("[\\[\\]\\s,]", ""));
        }
        return params;
    }
}
