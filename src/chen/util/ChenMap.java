package chen.util;

import java.util.Date;
import java.util.HashMap;

public class ChenMap extends HashMap<String, Object> {

    public ChenMap() {
        super();
    }

    public ChenMap(HashMap<String, Object> map) {
        super();
        this.putAll(map);
    }

    public int getInt(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,int.class):null;
    }

    public float getFloat(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,float.class):null;
    }

    public long getLong(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,long.class):null;
    }

    public double getDouble(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,double.class):null;
    }

    public boolean getBoolean(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,boolean.class):null;
    }

    public String getString(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,String.class):null;
    }

    public Date getTime(String key) {
        Object value = super.get(key);
        return value != null ? this.converter(value,Date.class):null;
    }

    public <T> T getAs(String key, Class<T> custom) {
    	Object value = super.get(key);
        return value != null ? this.converter(value,custom):null;
    }
    
    private <T> T converter(Object value, Class<T> t){
   		return (T) value;
    }
    
/*    public static void main(String[] args){
    	ChenMap tm = new ChenMap();
    	tm.put("a", 1);
    	tm.put("b","a2");
    	tm.put("c",0.12f);
    	
    	int a = tm.getInt("a");
    	String b = tm.getString("b");
    	float c = tm.getFloat("c");
    	String d = tm.getAs("b", String.class);
    	
    	System.out.println(a);
    	System.out.println(b);
    	System.out.println(c);
    	System.out.println(d);
    }*/
}