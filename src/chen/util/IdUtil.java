package chen.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class IdUtil{
	public static String uuid(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	public static String time(){
		return new SimpleDateFormat("yyyyMMddHHmmssSSSS").format(new Date());
	}
}