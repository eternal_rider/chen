package web;

import java.util.Map;

import chen.aop.Aop;
import chen.util.ContextUtil;

public class Test2Aop implements Aop {

	@Override
	public void before(Map<String,Object> args){
		Object user = ContextUtil.getContext().getHttpSession().getAttribute("UserInfo");
		if(user != null){
			Map<String,String> map = (Map<String,String>)user;
			System.out.println(map.get("USER_NAME")+"访问。全局Aop "+this.getClass().getName()+".before");
		}else{
			System.out.println("未地登录");
		}
	}

	@Override
	public void after(Map<String,Object> args){
		System.out.println("全局Aop "+this.getClass().getName()+".after");
	}
}
