package web;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

import chen.ioc.Chen;
import chen.util.ChenMap;
import chen.view.JspRenderer;
import chen.view.Renderer;

public class TinyTestAction {
	public void hello(Map args){
		
		System.out.println("aa："+((ChenMap)args).getString("aa"));
		System.out.println("访问时间1："+System.currentTimeMillis());
		//ContextUtil.getContext().getXXX;
	}
	
	public String hello2(Map<String,String> args){
		return "/index.jsp";
	}
	
	public Renderer hello3(Map<String,Object> args){
		//数据库
		//Map<String,Object> data = Chen.container.get(UserModel.class).outStr(args);
		Map<String,Object> data = new ChenMap();
		data.put("name", "222222");
		return new JspRenderer("/index.jsp",data);
	}
	public void hello4(Map<String,Object> args){
		Chen.container.get(UserModel.class).str();
	}
	
	public static void main(String[] args){
/*		System.out.println("向 chen 添加UserModel.clall 前");
		Chen.container.add(UserModel.class);
		System.out.println("向 chen 添加UserModel.clall 后");
		System.out.println("从 chen get UserModel.clall 前");
		Chen.container.get(UserModel.class).str();
		System.out.println("从 chen get UserModel.clall 后1");
		Chen.container.get(UserModel.class).str();
		System.out.println("从 chen get UserModel.clall 后2");*/
			try {
				String sourceURL = "jdbc:h2:file:E:/work/workspace2014/mydb";
				try {
					Class.forName("org.h2.Driver");// HSQLDB Driver
				} catch (Exception e) {
					e.printStackTrace();
				}
				Connection conn = DriverManager.getConnection(sourceURL, "admin", "123456");
				Statement stmt = conn.createStatement();
				stmt.execute("CREATE TABLE rules(user_code VARCHAR2(50),field VARCHAR2(50),name VARCHAR2(100));");
				stmt.execute("INSERT INTO rules VALUES(1,'MuSoft')");
				stmt.execute("INSERT INTO rules VALUES(2,'StevenStander')");
				stmt.close();
				conn.close();

			} catch (SQLException sqle) {
				sqle.printStackTrace();
			}
	}
}