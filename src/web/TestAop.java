package web;

import java.util.Map;

import chen.aop.Aop;

public class TestAop implements Aop {

	@Override
	public void before(Map<String,Object> args){
		System.out.println(this.getClass().getName()+".before");
	}

	@Override
	public void after(Map<String,Object> args){
		System.out.println(this.getClass().getName()+".after");
	}
}
