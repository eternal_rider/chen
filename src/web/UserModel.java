package web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import chen.ioc.Chen;
import chen.repository.OracleRepository;
import chen.util.DbUtil;

public class UserModel {
	static{
		System.out.println("UserModel static执行");
	}
	public UserModel(){
		System.out.println("UserModel 构造函数执行");
	}
	public Map<String,Object> outStr(Map<String,Object> params){
		try {
			//验证，参数转换
			Map<String,Object> args = new HashMap();
			
			//访问数据库
			Map<String,Object> data = DbUtil.dao.load(this, args);
			
			//业务逻辑处理
			return data;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	public void str(){
		System.out.println("你好ioc");
	}
}
