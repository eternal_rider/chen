package my.model;

import java.sql.SQLException;
import java.util.Map;

import chen.util.DbUtil;

public class UserModel {
	public Map<String,Object> login(Map<String,Object> args){
		try {
			Map<String,Object> userInfo = DbUtil.dao.load(loginSql,args);
			return userInfo;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
	private String loginSql = "select * from user t where USER_ACCT=#account# and USER_PWD=#password#";
}