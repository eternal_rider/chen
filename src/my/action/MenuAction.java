package my.action;

import java.util.Map;

import my.model.MenuModel;
import chen.ioc.Chen;
import chen.view.Renderer;
import chen.view.TextRenderer;

public class MenuAction {
	
	public Renderer list(Map<String,Object> args){
		MenuModel mmodel = Chen.container.get(MenuModel.class);
		String jsonStr = mmodel.loadMenuById(args);
		return new TextRenderer(jsonStr);
	}
	
	public Renderer add(Map<String,Object> args){
		MenuModel mmodel = Chen.container.get(MenuModel.class);
		int n = mmodel.add(args);
		return new TextRenderer(String.valueOf(n));
	}
	
	public Renderer edit(Map<String,Object> args){
		MenuModel mmodel = Chen.container.get(MenuModel.class);
		int n = mmodel.edit(args);
		return new TextRenderer(String.valueOf(n));
	}
	
	public Renderer delete(Map<String,Object> args){
		MenuModel mmodel = Chen.container.get(MenuModel.class);
		int n = mmodel.delete(args);
		return new TextRenderer(String.valueOf(n));
	}
}