package my.action;

import java.util.Map;

import javax.servlet.http.HttpSession;

import my.model.MenuModel;
import my.model.UserModel;
import chen.ioc.Chen;
import chen.util.ContextUtil;
import chen.view.JspRenderer;
import chen.view.Renderer;

public class HomeAction {
	
	public Renderer index(Map<String,Object> args){
/*		if(args == null && ContextUtil.getContext().getHttpSession().getAttribute("UserInfo") != null){
			return new JspRenderer("/pages/frame.jsp");
		}*/
		UserModel model = Chen.container.get(UserModel.class);
		Map<String,Object> data = model.login(args);
		if(data != null){
			MenuModel mmodel = Chen.container.get(MenuModel.class);
			String jsonStr = mmodel.loadAllMenu(args);
			data.put("MenuJson", jsonStr);
			return new JspRenderer("/pages/frame.jsp","UserInfo",data,"session");
		}else{
			return new JspRenderer("/index.jsp","TipMsg","用户名或密码错误");
		}
	}
	
	public String logOut(Map<String,Object> args){
		HttpSession session = ContextUtil.getContext().getHttpSession();
		session.removeAttribute("UserInfo");
		session.invalidate();
		return "/index.jsp";
	}
}