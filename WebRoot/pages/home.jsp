<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<!DOCTYPE HTML>
<html>
<head>
	<base href="${c:base()}">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>CHEN框架介绍</title>
</head>
<body>
<p class="theme-title"><strong>介绍</strong></p>
<p>
	<pre class="theme-title">
	chen框架是一个简单易用功能强大的web框架，包含mvc、aop、ioc、orm等功能。
	chen的设计本来只是突发奇想，想弄个最简单的mvc，只有3个类，虽然足够简单，但在使用时却存在诸多问题，后来我重新设计了chen，现在为v2版本。
	为了能让大家更好的了解chen的功能和使用，我开发了这个示例项目，这个示例项目包含完整的应用框架功能，如用户管理、角色管理、菜单管理、权限管理、日志管理、系统主题、框架属性自定义等。
	这个示例项目将支持多数据库oracle、mysql、h2，目前仅支持h2，其他正在开发中，项目的功能也在不断开发中。除了包含上述功能，还包含chen框架的设计文档、API文档、教程说明等。
	</pre>
</p>
<p class="theme-title"><strong>框架功能</strong></p>
<p>
	<pre class="theme-title">
	restful地址支持（chen中叫路由）。
	mvc功能，使用简单。
	多视图支持，支持自定义扩展。
	action支持同步和异步请求。
	ioc微容器。
	aop功能。
	文件自动上传
	充血模型。
	orm功能。
	数据库池。
	
	Ioc容器，管理着所有action和model类，框架使用者同样可以使用ioc。 
	多数据库支持，通过工厂模式支持多数据库实现，支持自定义扩展。
	orm功能使用简单，无需定义xml、注解、bean等，自动映射。
	简单，核心类只有10几个。
	</pre>
</p>
<p class="theme-title"><strong>整体处理流程如下：</strong></p>
<p>
	<pre class="theme-title">
	1、用户在页面发起了一个action的请求，前置控制器截获了这个请求，验证之后，将请求地址转化为路由。
	2、不同请求类型，如同步、异步或其他，由不同的handler处理，handler可以自定义扩展。
	3、生成执行环境，包括：servlet环境、路由、action实例、方法、参数等，将执行环境放入handler中。
	4、handler依次执行：初始化、执行、渲染视图、销毁资源4个步骤处理请求，有上传时自动上传，action从ioc容器中获取。
	5、如果有aop，则在执行action前后，执行aop的before和after方法。
	6、action类中一般调用的model类，处理业务逻辑，model类同样从ioc容器获得。
	7、model使用dao工具类，操作数据库，然后将结果返回给action。
	8、action中选择对应的视图，并将结果和视图交给视图渲染者。
	9、视图渲染者，转化、保存结果数据，返回到视图。
	</pre>
</p>
</body>
</html>
