<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<!doctype html>
<html>
<head>
<base href="${c:base()}">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>菜单管理</title>
	<%@ include file="/pages/manager/lib.jsp"%>
<script type="text/javascript">
var index = 0;
$(function(){
	$('#MenuGrid').treegrid({
				nowrap: false,
				collapsible:true,
				fit:true,
				title:"菜单管理",
				rownumbers: true,
				animate:true,
				url:'ty/menu/list/',
				idField:'id',
				treeField:'name',
				columns:[[
					{field:'name',title:'菜单名称',width:300},
					{field:'url',title:'访问路径',width:400}
				]],
		onContextMenu: function(e,row){
			e.preventDefault();
			$(this).treegrid('unselectAll');
			$(this).treegrid('select', row.id);
			if(row.leaf=='1'){
				$("#mm_add_menu").attr("disabled","disabled");
				$("#mm_add_dir").attr("disabled","disabled");
			}else{
				$("#mm_add_menu").removeAttr("disabled");
				$("#mm_add_dir").removeAttr("disabled");
			}
			$('#cm').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
		onLoadSuccess:function (row){
			if(index++<1){
				$('#MenuGrid').treegrid('expandAll');
			}
		}
	});
});
function getSelected(){
	return $('#MenuGrid').treegrid('getSelected');
}

function add(flag){
	var node = getSelected();
	if(node.leaf == '1'){
		$.messager.alert('警告', '不能在功能节点上添加！', 'warning');
		return;
	}
	if(flag == 1){
		$('#win').load('pages/manager/menu/moduleCreator.jsp',null,function(){
			$(this).window({title: '&nbsp;添加目录',iconCls:'icon-add'});
			$(this).window('open');
		});
	}else{
		$('#win').load('pages/manager/menu/menuCreator.jsp?fid=aq1',null,function(){
			$(this).window({title: '&nbsp;添加菜单',iconCls:'icon-add'});
			$(this).window('open');
		});
	}
}

function dele(){
	var node = getSelected();
	var b = $('#MenuGrid').treegrid('isLeaf', node.target);
	if(b){
		$.messager.confirm('信息', '是否确认删除[' + node.name + ']？', function(r){
			if(r){
				$.post('ty/menu/delete/',{id:node.id},function (data){
					if(data >= 1){
						$('#MenuGrid').treegrid('reload',node.pid);
					}else{
						$.messager.alert('错误', '删除失败，请联系管理员！', 'error');
					}
				});
			}
		});
	}else{
		$.messager.alert('警告', '请先删除子节点！', 'warning');
	}
}

function edit(){
	var node = getSelected();
		if(node.leaf==1){
			$('#win').load('pages/manager/menu/menuEditor.jsp',{id:node.id},function(){
				$(this).window({title: '&nbsp;编辑菜单',iconCls:'icon-add'});
				$(this).window('open');
			});
		}else{
			$('#win').load('pages/manager/menu/moduleEditor.jsp',null,function(){
				$(this).window({title: '&nbsp;编辑目录',iconCls:'icon-add'});
				$(this).window('open');
			});
		}
}
</script>
</head>
	<body topmargin="0" leftmargin="1" rightmargin="1">
			<table id="MenuGrid"></table>
			<div id="cm" class="easyui-menu" style="width: 120px;">
				<div id="mm_add_menu" onclick="add()" iconCls="icon-add">
					添加模块
				</div>
				<div id="mm_add_dir" onclick="add()" iconCls="icon-add">
					添加功能
				</div>
				<div class="menu-sep"></div>
				<div id="mm_dele" onclick="dele()" iconCls="icon-remove">
					删除
				</div>
				<div id="mm_edit" onclick="edit()" iconCls="icon-edit">
					编辑
				</div>
			</div>
		<div id="win" style="width: 650px; height: 300px; padding: 10px;" closed="true" shadow="true" resizable="false" collapsible="true" minimizable="false" maximizable="false"></div>
	</body>
</html>


