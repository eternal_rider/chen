<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<form id="editModule" method="post">
		<table width="100%">
			<tr>
				<td align="right" width="40%">
					<label for="pid">
						上级功能编号：
					</label>
				</td>
				<td width="60%" align="left">
					<input type="text" id="pid" name="pid" style="width: 300px" readonly="readonly" />
				</td>
			</tr>
			<tr>
				<td align="right">
					<label for="name">
						功能名称：
					</label>
				</td>
				<td align="left">
					<input type="text" id="name" name="name" style="width: 300px" />
				</td>
			</tr>
			<tr>
				<td align="right">
					<label for="ord">
						功能顺序：
					</label>
				</td>
				<td align="left">
					<input onKeyUp="this.value=this.value.replace(/\D/g,'')"
						type="text" id="ord" name="ord" style="width: 80px" />
					<input type="hidden" id="id" name="id" />
					<input type="hidden" name="url" value="#" />
					<input type="hidden" name="leaf" value="0" />
				</td>
			</tr>
		</table>
	</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#editModule').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
	$(function(){
		var selectd = getSelected();
		$('#editModule').form({
			url:'ty/menu/edit/',
    		success:function(data){
    			if(data >=1){
       				$.messager.alert('信息提示', '修改成功', 'info');
	       			$('#MenuGrid').treegrid('reload',selectd.pid);
	       			$('#win').window('close');
    			}
    		}
		});
		$("#id").val(selectd.id);
		$("#pid").val(selectd.pid);
		$("#name").val(selectd.name);
		$("#ord").val(selectd.ord);
		$('#save').linkbutton();
		$('#close').linkbutton();
	});
</script>