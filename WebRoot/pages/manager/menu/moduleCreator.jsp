<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<form id="addModule" method="post">
	<table width="100%">
		<tr>
			<td width="40%" align="right">
				<label for="pid">
					上级功能编号：
				</label>
			</td>
			<td align="left"  width="60%">
				<input type="text" id="pid" name="pid" style="width: 300px" readonly="readonly" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="name">
					功能名称：
				</label>
			</td>
			<td align="left">
				<input type="text" id="name" name="name" style="width: 300px" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="ord">
					功能顺序：
				</label>
			</td>
			<td align="left">
				<input onKeyUp="this.value=this.value.replace(/\D/g,'')" type="text" id="ord" name="ord" style="width: 80px" />
				<input type="hidden" name="id" value="${c:uuid()}" />
				<input type="hidden" name="url" value="directory" />
				<input type="hidden" name="leaf" value="0" />
			</td>
		</tr>
	</table>
</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#addModule').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
	$(function(){
		$('#addModule').form({
			url:'ty/menu/add/',
    		success:function(data){
    			if(data >=1){
       				$.messager.alert('信息提示', '添加成功', 'info');
	       			$('#MenuGrid').treegrid('reload',getSelected().id);
	       			$('#win').window('close');
    			}
    		}
		});
		$("#pid").val(getSelected().id);
		$('#save').linkbutton();
		$('#close').linkbutton();
	});
</script>