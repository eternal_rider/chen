<%@ page language="java" pageEncoding="UTF-8"%>
<form id="editMenu" method="post">
	<table width="100%">
		<tr>
			<td width="40%" align="right">
				<label for="pid">
					上级功能编号：
				</label>
			</td>
			<td align="left"  width="60%">
				<input type="text"  id="pid" name="pid"	style="width: 300px" readonly="readonly" />只读
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="name">
					功能名称：
				</label>
			</td>
			<td align="left">
				<input type="text" id="name" name="name"  style="width: 300px" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="ord">
					功能顺序：
				</label>
			</td>
			<td align="left">
				<input onKeyUp="this.value=this.value.replace(/\D/g,'')"
					type="text" id="funOrder" name="funOrder" style="width: 80px" />
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="url">
					功能地址：
				</label>
			</td>
			<td align="left">
				<input type="text" id="url" name="url" style="width: 300px" />
				<input type="hidden" id="id" name="id" />
				<input type="hidden" id="leaf" name="leaf"/>
			</td>
		</tr>
	</table>
</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#editMenu').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
	$(function(){
		var selectd = getSelected();
		$('#editMenu').form({
			url:'ty/menu/edit/',
    		success:function(data){
    			if(data >=1){
       				$.messager.alert('信息提示', '修改成功', 'info');
       				$('#MenuGrid').treegrid('reload',selectd.pid);
       				$('#win').window('close');
    			}else{
    				$.messager.alert('信息提示', '修改失败', 'error');
    			}
    		}
		});
		$("#id").val(selectd.id);
		$("#pid").val(selectd.pid);
		$("#name").val(selectd.name);
		$("#ord").val(selectd.ord);
		$("#url").val(selectd.url);
		$("#leaf").val(selectd.leaf);
		
		$('#save').linkbutton();
		$('#close').linkbutton();
	});
</script>