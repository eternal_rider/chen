<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<e:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>用户管理</title>
	<%@ include file="/pages/frame/common/res.jsp"%>
	<script type="text/javascript">
	 $(function(){
	     $('#NoticeGrid').datagrid({
	     	 title:'公告管理',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 animate:true,
			 idField:'NOTICE_ID',
			 checkbox:true,
	         url:'notice/list',
			 toolbar:'#notice_toolbar',
			 frozenColumns:[[
                {field:'ck',checkbox:true}
			 ]],
	         columns:[[
	             {field:'NOTICE_ID',title:'编号',width:100},
	             {field:'TITLE',title:'标题',width:150},
	             {field:'START_DAY',title:'开始时间',width:150},
	             {field:'END_DAY',title:'结束时间',width:240},
				 {field:'STATE',title:'状态',width:50},
				 {field:'USER_NAME',title:'发布人',width:150},
				 {field:'opt',title:'操作',width:100,formatter:function(value,rec){
					return '<a href="javascript:void(0);" onclick="openNoticeWin(\''+rec.TITLE+'\',\''+rec.CONTENT+'\');" style="color:red">查看</a>'+
						   '<span style="padding-left:5px"></span>'+
						   '<a href="notice/edit/'+rec.NOTICE_ID+'" style="color:red">修改</a>';
				}}
	         ]],
	         pagination:true
	     });
	 });
	function getSelecteds(){
		return $('#NoticeGrid').datagrid('getChecked');
	}
	function deleteNotices(){
		var tips = [];
		var ids = [];
		var rows = getSelecteds();
		if(rows.length>0){
			for(var i=0;i<rows.length;i++){
				var n = i+1;
				ids.push('\''+rows[i].NOTICE_ID+'\'');
				tips.push(n+'、'+rows[i].TITLE);
			}
			$.messager.confirm('信息提示', '您确认删除以下公告吗？<br/>'+tips.join('<br/>'), function(r){
				if(r){
					$.post('user/delete/'+ids.join(),function(data){
						$('#NoticeGrid').datagrid('clearChecked');
						$('#NoticeGrid').datagrid('reload');
					});
				}
			});
		}else{
			$.messager.alert('警告', '请先选择要删除的公告！', 'warning');
		}
	}
	function openNoticeWin(tt,cc){
		$('#win').html(cc);
		$('#win').window({title: '&nbsp;'+tt,width:600,height:400,iconCls:'icon-ok'});
		$('#win').window('open');
	}
	function searchKey(value,name){
		var params = {key:value};
		$('#NoticeGrid').datagrid('options').queryParams=params;
		$('#NoticeGrid').datagrid('reload');
	}
	function gridRefresh(){
	$('#NoticeGrid').datagrid('options').queryParams='';
		$('#NoticeGrid').datagrid('reload');
	}
	</script>
</head>
<body topmargin="1" leftmargin="1">
	<table id="NoticeGrid"></table>
	<div id="notice_toolbar" style="padding:2px 0">
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="padding-left:2px">
					<a href="pages/frame/notice/jsp/noticeCreate.jsp" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
					<a href="javascript:void(0);" onclick="deleteNotices();" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
				</td>
				<td style="text-align:right;padding-right:2px">
					<input class="easyui-searchbox" searcher="searchKey" prompt="请输入公告信息" menu="#search_user"></input>
				</td>
			</tr>
		</table>
	</div>
	<div id="search_user" style="width:120px">
		<div name="name" iconCls="icon-search">按标题</div>
	</div>
	<div id="win" style="padding: 1px;" closed="true" shadow="true" resizable="false" collapsible="true" minimizable="false" maximizable="false"></div>
</body>
</html>