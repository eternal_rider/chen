<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<e:base/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>随风流逝的岁月</title>
	<style>
		*{
			font-size:12px;
		}
	</style>
	<link rel="stylesheet" href="pages/frame/notice/themes/default/default.css" />
	<link rel="stylesheet" href="pages/frame/notice/plugins/code/prettify.css" />
	<link rel="stylesheet" type="text/css" href="resources/themes/gray/easyui.css"/>
	<link rel="stylesheet" type="text/css" href="resources/themes/icon.css"/>
	<script type="text/javascript" src="resources/lib/jquery-1.7.2.min.js"></script>
	<script charset="utf-8" src="pages/frame/notice/kindeditor-min.js"></script>
	<script charset="utf-8" src="pages/frame/notice/lang/zh_CN.js"></script>
	<script charset="utf-8" src="pages/frame/notice/plugins/code/prettify.js"></script>
	<script type="text/javascript" src="resources/lib/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="resources/lib/easyui-lang-zh_CN.js"></script>
	<script>
		KindEditor.ready(function(K) {
			var editor1 = K.create('textarea[name="note_text"]', {
				cssPath : 'pages/frame/notice/plugins/code/prettify.css',
				uploadJson : 'pages/frame/notice/jsp/upload_json.jsp',
				fileManagerJson : 'pages/frame/notice/jsp/file_manager_json.jsp',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						self.sync();
						document.forms['editor_form'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						self.sync();
						document.forms['editor_form'].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
</head>
<body topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top">
	<form id="editor_form" name="editor_form" method="post" action="notice/add">
	<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr bgcolor="#ECECEA"><td valign="top">
	  	<span style="padding-left:2px;"></span>
	  	<label>标题：<input type="text" id="title" name="title" style="width:200px"></label><span style="padding-left:20px;"></span>
	  	<label>开始日期：<input id="start_day" name="start_day" class="easyui-datebox"></input></label>
	  	<span style="padding-left:20px;"></span>
	  	<label>结束日期：<input id="end_day" name="end_day" class="easyui-datebox"></input></label>
	  	<span style="padding-left:20px;"></span>
	  	<a href="javascript:void(0);" onclick="javascript:document.forms['editor_form'].submit();" class="easyui-linkbutton">确定，添加</a>
	  </td></tr>
	  <tr><td valign="top">
	  	<textarea name="note_text" style="width:100%;height:400px;visibility:hidden;"></textarea>
	  </td></tr>
	</table>	
	</form>
    </td>
  </tr>
</table>
</body>
</html>