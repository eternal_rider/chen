<%@ page language="java" pageEncoding="UTF-8"%>
<form id="addUser" method="post">
		<table width="100%">
		<tr>
			<td width="40%" align="right">
				<label for="account">
					用户帐号：
				</label>
			</td>
			<td align="left" width="60%">
				<input id="account" name="account" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="user_name">
					用户名称：
				</label>
			</td>
			<td align="left">
				<input id="user_name" name="user_name" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="password">
					用户密码：
				</label>
			</td>
			<td align="left">
				<input id="password" name="password" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="sex">
					用户性别：
				</label>
			</td>
			<td align="left">
				<input id="sex" name="sex" style="width: 135px;" class="e_input"/> 0女，1男
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="office_phone">
					办公电话：
				</label>
			</td>
			<td align="left">
				<input id="office_phone" name="office_phone" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="mobile_phone">
					手机电话：
				</label>
			</td>
			<td align="left">
				<input id="mobile_phone" name="mobile_phone" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="email">
					邮件地址：
				</label>
			</td>
			<td align="left">
				<input id="email" name="email" style="width: 135px;" class="e_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="demo">
					备注：
				</label>
			</td>
			<td align="left">
				<textarea id="demo" name="demo" style="width: 400px;height:50px" class="e_input"></textarea>
			</td>
		</tr>
	</table>
</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#addUser').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
$(function(){
	$('#addUser').form({
		url:'user/add',
   		success:function(data){
      			$.messager.alert('信息提示', data, 'info');
      			$('#UserGrid').datagrid('reload');
      			$('#win').window('close');;
   		}
	});
	$('#save').linkbutton();
	$('#close').linkbutton();
});
</script>