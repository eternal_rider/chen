<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<!doctype html>
<html>
<head>
<base href="${c:base()}">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>用户管理</title>
	<%@ include file="/pages/manager/lib.jsp"%>
	<script type="text/javascript">
	 var curSelectData;
	 $(function(){
	     $('#UserGrid').datagrid({
	     	 title:'用户管理',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 animate:true,
			 checkbox:true,
			 idField:'USER_ID',
	         url:'user/list',
			 toolbar:'#user_toolbar',
			 frozenColumns:[[
		                {field:'ck',checkbox:true}
					]],
	         columns:[[
	             {field:'USER_ID',title:'编号',width:100},
	             {field:'USER_ACCT',title:'帐号',width:150},
	             {field:'USER_NAME',title:'名称',width:150},
	             {field:'USER_PWD',title:'密码',width:240},
				 {field:'CREATOR',title:'创建人',width:50},
				 {field:'CREATE_TIME',title:'创建日期',width:150},
				 {field:'opt',title:'操作',width:100,formatter:function(value,rec){
								return '<a href="pages/frame/user/userRoleAssign.jsp?id='+rec.USER_ID+'"  style="color:red">角色</a>'+
									   '<span style="padding-left:5px"></span>'+
									   '<a href="pages/frame/user/userMenuAssign.jsp?id='+rec.USER_ID+'" style="color:red">权限</a>'+
									   '<span style="padding-left:5px"></span>'+
									   '<a href="javascript:void(0);" onclick="openUserWin(\'edit\',\''+rec.USER_ID+'\');" style="color:red">修改</a>';
							}}
	         ]],
	         onClickRow:function(rowIndex, rowData){
				curSelectData = rowData;
	         },
	         pagination:true
	     });
	 });
	function getSelecteds(){
		return $('#UserGrid').datagrid('getChecked');
	}
	function deleteUsers(){
		var tips = [];
		var ids = [];
		var rows = getSelecteds();
		if(rows.length>0){
			for(var i=0;i<rows.length;i++){
				var n = i+1;
				ids.push('\''+rows[i].USER_ID+'\'');
				tips.push(n+'、'+rows[i].USER_NAME);
			}
			$.messager.confirm('信息提示', '您确认删除以下用户吗？<br/>'+tips.join('<br/>'), function(r){
				if(r){
					$.post('user/delete/'+ids.join(),function(data){
						$('#UserGrid').datagrid('clearChecked');
						$('#UserGrid').datagrid('reload');
					});
				}
			});
		}else{
			$.messager.alert('警告', '请先选择要删除的用户！', 'warning');
		}
	}
	function openUserWin(t,id){
		var info = {};
		if(t == 'add'){
			info.url = 'pages/frame/user/userCreator.jsp';
			info.title = '添加用户';
			info.icon = 'icon-add';
			info.height = 360;
		}
		if(t == 'role'){
			info.url = 'pages/frame/user/userRoleAssign.jsp';
			info.title = '用户所属角色';
			info.icon = '';
			info.height = 540;
		}
		if(t == 'edit'){
			info.url = 'pages/frame/user/userEditor.jsp';
			info.title = '编辑用户';
			info.icon = 'icon-edit';
			info.height = 360;
		}
		$('#win').load(info.url,{id:id},function(){
			$(this).window({title: '&nbsp;'+info.title,height:info.height,iconCls:info.icon});
			$(this).window('open');
		});
	}
	function searchKey(value,name){
		var params = {key:value};
		$('#UserGrid').datagrid('options').queryParams=params;
		$('#UserGrid').datagrid('reload');
	}
	function gridRefresh(){
	$('#UserGrid').datagrid('options').queryParams='';
		$('#UserGrid').datagrid('reload');
	}
	</script>
</head>
<body topmargin="1" leftmargin="1">
	<table id="UserGrid"></table>
	<div id="user_toolbar" style="padding:2px 0">
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="padding-left:2px">
					<a href="javascript:void(0);" onclick="openUserWin('add','');" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
					<a href="javascript:void(0);" onclick="gridRefresh();" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</a>
					<a href="javascript:void(0);" onclick="deleteUsers();" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
				</td>
				<td style="text-align:right;padding-right:2px">
					<input class="easyui-searchbox" searcher="searchKey" prompt="请输入用户信息" menu="#search_user"></input>
				</td>
			</tr>
		</table>
	</div>
	<div id="search_user" style="width:120px">
		<div name="name" iconCls="icon-search">按名称</div>
	</div>
	<div id="win" style="width: 650px; padding: 10px;" closed="true" shadow="true" resizable="false" collapsible="true" minimizable="false" maximizable="false"></div>
</body>
</html>