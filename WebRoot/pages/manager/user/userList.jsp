<%@ page language="java" pageEncoding="UTF-8"%>
	<script type="text/javascript">
	 var curSelectData;
	 $(function(){
	     $('#UserGrid').datagrid({
	     	 title:'',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 idField:'USER_ID',
	         url:'user/list',
	         columns:[[
	             {field:'USER_ACCT',title:'帐号',width:120},
	             {field:'USER_NAME',title:'名称',width:120},
				 {field:'CREATE_TIME',title:'创建日期',width:120},
				 {field:'opt',title:'操作',width:50,formatter:function(value,rec){
								return '<a href="javascript:void(0);" onclick="setUser(\''+rec.USER_ID+'\');" style="color:red">添加</a>';
							}}
	         ]],
	         pagination:true
	     });
	 });
	 function setUser(id){
	 	$('#${param.id}').val(id);
	 }
	</script>
<table id="UserGrid"></table>