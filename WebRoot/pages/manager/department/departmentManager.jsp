<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<e:base/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>部门管理</title>
<%@ include file="/pages/frame/common/res.jsp"%>
<script type="text/javascript">
var index = 0;
$(function(){
	$('#DeptGrid').treegrid({
				nowrap: false,
				title:'部门管理',
				collapsible:true,
				fit:true,
				rownumbers: true,
				animate:true,
				url:'/',
				idField:'DEPT_ID',
				treeField:'DEPT_NAME',
				columns:[[
					{field:'DEPT_NAME',title:'部门名称',width:240},
					{field:'DEPT_ADDRESS',title:'地址',width:300},
					{field:'USER_COUNT',title:'员工数(人)',width:80}
				]],
		onContextMenu: function(e,row){
			e.preventDefault();
			$(this).treegrid('unselectAll');
			$(this).treegrid('select', row.DEPT_ID);
			if(row.DEPT_ID==10000){
				$("#cm_append").attr("disabled",false);
				$("#cm_remove").attr("disabled",true);
				$("#cm_edit").attr("disabled",true);
			}else{
				$("#cm_append").attr("disabled",false);
				$("#cm_remove").attr("disabled",false);
				$("#cm_edit").attr("disabled",false);
			}
			$('#cm').menu('show', {
				left: e.pageX,
				top: e.pageY
			});
		},
		onBeforeLoad: function(node, param){
			if(node == null){
				$(this).treegrid('options').url = 'detpJson/-1';
			}else{
				$(this).treegrid('options').url = 'detpJson/'+node.DEPT_ID;
			}
		},
		onLoadSuccess:function (row){
			if(index++<=1){
				$('#DeptGrid').treegrid('expandAll');
			}
		}
	});
});

function getSelected(){
	return $('#DeptGrid').treegrid('getSelected');
}

function append(){
	var node = getSelected();
	$('#win').load('pages/frame/department/departmentCreator.jsp',null,function(){
		$(this).window({title: '&nbsp;添加部门',iconCls:'icon-add'});
		$(this).window('open');
	});
}

function remove(){
	var node = getSelected();
	var info = {};
	if(node.DEPT_ID!=10000){
		$.messager.confirm('信息', '是否确认删除[' + node.DEPT_NAME + ']？', function(r){
			if(r){
				info.eaction = "remove";
				info.id = node.DEPT_ID;
				$.post('deptRemove.ot', info,function (data){
					$('#DeptGrid').treegrid('reload');
				});
			}
		});
	}else{
		$.messager.alert('警告', '不能删除！', 'warning');
	}
}

function edit(){
	var node = getSelected();
	$('#win').load('pages/frame/department/departmentEditor.jsp',null,function(){
		$(this).window({title: '&nbsp;编辑部门',iconCls:'icon-edit'});
		$(this).window('open');
	});
}
</script>
</head>
	<body>
		<table id="DeptGrid"></table>
		<div id="cm" class="easyui-menu" style="width: 120px;">
			<div id="cm_append" onclick="append()" iconCls="icon-add">
				创建部门
			</div>
			<div id="cm_remove" onclick="remove()" iconCls="icon-remove">
				删除部门
			</div>
			<div id="cm_edit" onclick="edit()" iconCls="icon-edit">
				编辑部门
			</div>
		</div>
	<div id="win" style="width: 650px; height: 300px; padding: 10px;" closed="true" shadow="true" resizable="false" collapsible="true" minimizable="false" maximizable="false"></div>
	</body>
</html>


