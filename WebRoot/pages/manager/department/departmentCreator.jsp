<%@ page language="java" pageEncoding="UTF-8"%>
<form id="addDept" method="post">
	<table style="width: 100%">
		<tr>
			<td style="width: 100px; text-align: right;">
				<label for="parentDeptId">
					上级部门编号：
				</label>
			</td>
			<td>
				<input type="text" id="parentDeptId" name="parentDeptId"
					style="width: 135px" readonly="readonly" class="ot_input"/>
			</td>
			<td style="width: 90px; text-align: right;">
				<label for="areaNo">
					地域：
				</label>
			</td>
			<td>
				<a:area id="areaNo" name="areaNo" areaNo="${sessionScope.user.area_no}"/>
			</td>
		</tr>
		<tr>
			<td style="width: 90px; text-align: right;">
				<label for="deptName">
					部门名称：
				</label>
			</td>
			<td>
				<input type="text" id="deptName" name="deptName"  class="ot_input"
					style="width: 135px" />
			</td>
			<td style="width: 90px; text-align: right;">
				<label for="deptOrder">
					部门顺序：
				</label>
			</td>
			<td>
				<input onKeyUp="this.value=this.value.replace(/\D/g,'')"
					type="text" id="deptOrder" name="deptOrder"  class="ot_input"
					style="width: 130px" />
			</td>
		</tr>
		
		<tr>
			<td style="width: 90px; text-align: right;">
				<label for="deptAdd">
					部门地址：
				</label>
			</td>
			<td colspan="3">
				<input type="text" id="deptAdd" name="deptAdd"  class="ot_input"
					style="width: 400px" />
			</td>
		</tr>
		<tr>
			<td style="width: 90px; text-align: right;">
				<label for="deptDesc">
					部门描述：
				</label>
			</td>
			<td colspan="3">
				<textarea id="deptDesc" name="deptDesc" style="width: 400px;height:50px" class="ot_input"></textarea>
				<input type="hidden" name="eaction" value="append" />
			</td>
		</tr>
	</table>
</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#addDept').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
	$(function(){
		$('#addDept').form({
			url:'dept/add"',
    		success:function(data){
       			$.messager.alert('信息提示', data, 'info');
       			$('#DeptGrid').treegrid('reload',getSelected().DEPT_ID);
       			$('#win').window('close');
    		}
		});
		$("#parentDeptId").val(getSelected().DEPT_ID);
		$('#save').linkbutton();
		$('#close').linkbutton();
	});
</script>