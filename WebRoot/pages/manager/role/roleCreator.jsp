<%@ page language="java" pageEncoding="UTF-8"%>
<form id="addRole" method="post">
		<table width="100%">
		<tr>
			<td width="40%" align="right">
				<label for="roleNo">
					角色编号：
				</label>
			</td>
			<td align="left" width="60%">
				<input id="roleNo" name="roleNo" style="width: 135px;" class="ot_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="roleName">
					角色名称：
				</label>
			</td>
			<td align="left">
				<input id="roleName" name="roleName" style="width: 135px;" class="ot_input"/>
			</td>
		</tr>
		<tr>
			<td align="right">
				<label for="roleDesc">
					备注：
				</label>
			</td>
			<td align="left">
				<textarea id="roleDesc" name="roleDesc" style="width: 400px;height:50px" class="ot_input"></textarea>
			</td>
		</tr>
	</table>
</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#addRole').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
$(function(){
	$('#addRole').form({
		url:'role/add',
   		success:function(data){
      			$.messager.alert('信息提示', data, 'info');
      			$('#RoleGrid').datagrid('reload');
      			$('#win').window('close');;
   		}
	});
	$('#save').linkbutton();
	$('#close').linkbutton();
});
</script>