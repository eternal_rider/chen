<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<e:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>角色管理</title>
	<%@ include file="/pages/frame/common/res.jsp"%>
	<script type="text/javascript">
	 var curSelectData;
	 $(function(){
	     $('#RoleGrid').datagrid({
	     	 title:'角色管理',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 animate:true,
			 checkbox:true,
			 idField:'ROLE_NO',
	         url:'role/list',
			 toolbar:'#role_toolbar',
			 frozenColumns:[[
		                {field:'ck',checkbox:true}
					]],
	         columns:[[
	             {field:'ROLE_NO',title:'角色编号',width:100},
	             {field:'ROLE_NAME',title:'名称',width:150},
	             {field:'ROLE_DESC',title:'说明',width:240},
				 {field:'STATE_NAME',title:'状态',width:50},
				 {field:'CREATE_TIME',title:'创建日期',width:150},
				 {field:'opt',title:'操作',width:100,formatter:function(value,rec){
								return '<a href="pages/frame/role/roleUserAssign.jsp?id='+rec.ROLE_NO+'" style="color:red">人员</a>'+
									   '<span style="padding-left:5px"></span>'+
									   '<a href="pages/frame/role/roleMenuAssign.jsp?id='+rec.ROLE_NO+'" style="color:red">权限</a>'+
									   '<span style="padding-left:5px"></span>'+
									   '<a href="javascript:void(0);" onclick="openRoleWin(\'edit\',\''+rec.ROLE_NO+'\');" style="color:red">修改</a>';
							}}
	         ]],
	         onClickRow:function(rowIndex, rowData){
				curSelectData = rowData;
	         },
	         pagination:true
	     });
	 });
	function getSelecteds(){
		return $('#RoleGrid').datagrid('getChecked');
	}
	function deleteRoles(){
		var tips = [];
		var ids = [];
		var rows = getSelecteds();
		if(rows.length>0){
			for(var i=0;i<rows.length;i++){
				var n = i+1;
				ids.push('\''+rows[i].ROLE_NO+'\'');
				tips.push(n+'、'+rows[i].ROLE_NAME);
			}
			$.messager.confirm('信息提示', '您确认删除以下角色吗？<br/>'+tips.join('<br/>'), function(r){
				if(r){
					$.post('role/delete/'+ids.join(),function(data){
						$('#RoleGrid').datagrid('clearChecked');
						$('#RoleGrid').datagrid('reload');
					});
				}
			});
		}else{
			$.messager.alert('警告', '请先选择要删除的角色！', 'warning');
		}
	}
	function openRoleWin(t,id){
		var info = {};
		if(t == 'add'){
			info.url = 'pages/frame/role/roleCreator.jsp';
			info.title = '添加角色';
			info.icon = 'icon-add';
			info.height = 300;
		}
		if(t == 'edit'){
			info.url = 'pages/frame/role/roleEditor.jsp';
			info.title = '编辑角色';
			info.icon = 'icon-edit';
			info.height = 300;
		}
		$('#win').load(info.url,{id:id},function(){
			$(this).window({title: '&nbsp;'+info.title,height:info.height,iconCls:info.icon});
			$(this).window('open');
		});
	}
	function searchKey(value,name){
		var params = {key:value};
		$('#RoleGrid').datagrid('options').queryParams=params;
		$('#RoleGrid').datagrid('reload');
	}
	function gridRefresh(){
	$('#RoleGrid').datagrid('options').queryParams='';
		$('#RoleGrid').datagrid('reload');
	}
	</script>
</head>
<body topmargin="1" leftmargin="1">
	<table id="RoleGrid"></table>
	<div id="role_toolbar" style="padding:2px 0">
		<table cellpadding="0" cellspacing="0" style="width:100%">
			<tr>
				<td style="padding-left:2px">
					<a href="javascript:void(0);" onclick="openRoleWin('add','');" class="easyui-linkbutton" iconCls="icon-add" plain="true">添加</a>
					<a href="javascript:void(0);" onclick="gridRefresh();" class="easyui-linkbutton" iconCls="icon-reload" plain="true">刷新</a>
					<a href="javascript:void(0);" onclick="deleteRoles();" class="easyui-linkbutton" iconCls="icon-remove" plain="true">删除</a>
				</td>
				<td style="text-align:right;padding-right:2px">
					<input class="easyui-searchbox" searcher="searchKey" prompt="请输入角色信息" menu="#search_menu"></input>
				</td>
			</tr>
		</table>
	</div>
	<div id="search_menu" style="width:120px">
		<div name="name" iconCls="icon-search">按名称</div>
	</div>
	<div id="win" style="width: 650px; padding: 10px;" closed="true" shadow="true" resizable="false" collapsible="true" minimizable="false" maximizable="false"></div>
</body>
</html>