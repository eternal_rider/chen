<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<e:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>角色授权</title>
	<%@ include file="/pages/frame/common/res.jsp"%>
	<script type="text/javascript">
		var index = 0;
		$(function(){
			$('#menu_tree').tree({
				checkbox: true,
				url:'/',
				onBeforeLoad: function(node, param){
					if(node == null){
						$(this).tree('options').url = 'menu/tree/root/all';
					}else{
						$(this).tree('options').url = 'menu/tree/'+node.id+'/all';
					}
				},
				onLoadSuccess:function (node, data){
					if(index <2){
						$('#menu_tree').tree('expandAll');
					}
					index++;
				}
			});
			$('#save').bind('click',function(){
				var selId = getChecked();
				$.post('role/power/${param.id}/'+selId,null,function(data){
					$.messager.alert('信息提示', data, 'info');
					$('#menu_tree').tree('reload');
				});
			});
		});
		
		function getChecked(){
			var nodes = $('#menu_tree').tree('getChecked');
			var ids = '';
			for(var i=0; i<nodes.length; i++){
				if (ids != '') ids += ',';
				ids = ids+''+nodes[i].id;
			}
			return ids;
		}
	</script>
</head>
<body topmargin="1" leftmargin="1">
<div class="easyui-panel" style="width:auto;height:auto;padding:0px;" data-options="title:'角色授权',iconCls:'icon-save',collapsible:true,minimizable:false,maximizable:true,closable:false">
<table border="0" height="100%" width="100%">
<tr>
	<td width="50%" valign="top"><ul id="menu_tree"></ul></td>
    <td width="50%" valign="top" align="center"><a href="pages/frame/role/roleManager.jsp" class="easyui-linkbutton">返回，角色管理</a><span style="padding-left:20px"></span><a id="save" href="javascript:void(0)" class="easyui-linkbutton">确定，保存</a></td>
</tr>
</table>
</div>
</body>
</html>