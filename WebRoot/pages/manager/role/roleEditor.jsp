<%@ page language="java" pageEncoding="UTF-8"%>
<form id="editRole" method="post">
	<table width="100%">
			<tr>
				<td width="40%" align="right">
					<label for="roleNo">
						角色编号：
					</label>
				</td>
				<td align="left" width="60%">
					<input id="roleNo" name="roleNo" style="width: 135px;" readonly="readonly" class="ot_input"/>只读
				</td>
			</tr>
			<tr>
				<td align="right">
					<label for="roleName">
						角色名称：
					</label>
				</td>
				<td align="left">
					<input id="roleName" name="roleName" style="width: 135px;" class="ot_input"/>
				</td>
			</tr>
			<tr>
				<td align="right">
					<label for="roleDesc">
						备注：
					</label>
				</td>
				<td align="left">
					<textarea style="width: 400px;height:50px" id="roleDesc" name="roleDesc" class="ot_input"></textarea>
				</td>
			</tr>
		</table>
	</form>
<p align="center">
	<a id="save" href="javascript:void(0);" onclick="javascript:$('#editRole').submit();" iconCls="icon-ok">保存</a>
	<span style="padding-left:20px;"></span>
	<a id="close" href="javascript:void(0);" onclick="javascript:$('#win').window('close');" iconCls="icon-cancel">关闭</a>
</p>
<script type="text/javascript">
	$(function(){
		$('#editRole').form({
			url:'role/edit/'+curSelectData.ROLE_NO,
    		success:function(data){
       			$.messager.alert('信息提示', data, 'info');
       			$('#RoleGrid').datagrid('reload');
       			$('#win').window('close');
    		}
		});
		$("#roleNo").val(curSelectData.ROLE_NO);
		$("#roleName").val(curSelectData.ROLE_NAME);
		$("#roleDesc").val(curSelectData.ROLE_DESC);
		$('#save').linkbutton();
		$('#close').linkbutton();
	});
</script>