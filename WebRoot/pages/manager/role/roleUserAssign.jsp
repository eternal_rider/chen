<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="e" uri="http://my.oschina.net/eternal/e" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<e:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>角色-用户</title>
	<%@ include file="/pages/frame/common/res.jsp"%>
	<script>
	    $(function(){
	     $('#own_user').datagrid({
	     	 title:'',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 animate:true,
			 checkbox:true,
			 idField:'USER_ID',
	         url:'user/role/list/${param.id}',
			 toolbar:'#own_user_toolbar',
	         columns:[[
	             {field:'USER_ID',title:'编号',width:100},
	             {field:'USER_ACCT',title:'帐号',width:150},
	             {field:'USER_NAME',title:'名称',width:150},
				 {field:'CREATOR',title:'创建人',width:100},
				 {field:'CREATE_TIME',title:'创建日期',width:150},
				 {field:'opt',title:'操作',width:100,formatter:function(value,rec){
					return '<a href="javascript:void(0);" onclick="userRoleRel(\'delete\',\''+rec.USER_ID+'\');" style="color:red">删除</a>';							}}
	         ]],
	         pagination:true
	     });
	     $('#no_add').datagrid({
	     	 title:'',
	     	 fit:true,
	     	 collapsible:true,
		     nowrap: false,
			 rownumbers: true,
			 animate:true,
			 checkbox:true,
			 idField:'USER_ID',
	         url:'user/role/no/list/${param.id}',
			 toolbar:'#no_add_toolbar',
	         columns:[[
	             {field:'USER_ID',title:'编号',width:100},
	             {field:'USER_ACCT',title:'帐号',width:150},
	             {field:'USER_NAME',title:'名称',width:150},
				 {field:'CREATOR',title:'创建人',width:100},
				 {field:'CREATE_TIME',title:'创建日期',width:150},
				 {field:'opt',title:'操作',width:100,formatter:function(value,rec){
					return '<a href="javascript:void(0);" onclick="userRoleRel(\'add\',\''+rec.USER_ID+'\');" style="color:red">添加</a>';							}}
	         ]],
	         pagination:true
	     });
	    });
		function userRoleRel(t,id){
			if(t == 'delete'){
				$.post('user/role/delete/${param.id}/'+id,function(data){
					$('#own_user').datagrid('reload');
					$('#no_add').datagrid('reload');
					alert(data);
				});
			}else if(t == 'add'){
				$.post('user/role/add/${param.id}/'+id,function(data){
					$('#own_user').datagrid('reload');
					$('#no_add').datagrid('reload');
					alert(data);
				});
			}else{
				alert('没有此操作');
			}
		}
		function searchKey(value,name){
			var params = {key:value};
			$('#own_user').datagrid('options').queryParams=params;
			$('#own_user').datagrid('reload');
		}
		function searchKey2(value,name){
			var params = {key:value};
			$('#no_add').datagrid('options').queryParams=params;
			$('#no_add').datagrid('reload');
		}
	</script>
	</head>
	<body topmargin="1" leftmargin="1">
	<center><a href="pages/frame/role/roleManager.jsp" class="easyui-linkbutton">返回，角色管理</a></center>
	<div id="role_user" class="easyui-tabs" style="width:auto;height:auto" data-options="tabPosition:'right',fit:true,plain:true">
		<div title="拥有的用户" style="padding:1px">
			<table id="own_user"></table>
		</div>
		<div title="添加用户" style="padding:1px">
			<table id="no_add"></table>
		</div>
	</div>	
    <div id="own_user_toolbar" style="padding:2px 0">
        <input class="easyui-searchbox" searcher="searchKey" prompt="请输入用户名称" menu="#search_menu"></input>
    </div>
    <div id="no_add_toolbar" style="padding:2px 0">
        <input class="easyui-searchbox" searcher="searchKey2" prompt="请输入用户名称" menu="#search_menu"></input>
    </div>
    <div id="search_menu" style="width:120px">
        <div name="name">按名称</div>
    </div>
	</body>
</html>