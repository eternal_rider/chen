<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<!doctype html>
<html>
<head>
	<base href="${c:base()}">
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${c:get("AppName")}</title>
    <!--[if lte IE 8]><script src="resources/box/js/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="resources/box/css/normalize.css">
    <link rel="stylesheet" href="resources/box/css/style.css">
    <link type="text/css" href="resources/menu/menu.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="resources/box/css/default.css" />
    <link rel="stylesheet" type="text/css" href="resources/box/css/lightwindow.css" />
    <script type="text/javascript" src="resources/jquery.min.js"></script>
	<script type="text/javascript" src="resources/box/js/prototype.js"></script>
	<script type="text/javascript" src="resources/box/js/effects.js"></script>
	<script type="text/javascript" src="resources/box/js/lightwindow.js"></script>
    <script type="text/javascript" src="resources/menu/menu.js"></script>
    <script type="text/javascript">
    jQuery(function(){
    	jQuery("#mymenu").menu('${sessionScope.UserInfo.MenuJson}');
    	});
    </script>
</head>
</head>
<body>
    <header style=" background-color:#545454;">
        <a class="logo" href="javascript:void(0);"><h1>CHEN</h1></a>
        <p class="logoText">欢迎，${sessionScope.UserInfo.USER_NAME}</p>
		<div id="mymenu" class="mainMenu"></div>
    </header>
    <div class="wrapMain">
        <div role="main" class="main">
            <div class="filter">
                <em class="filterName">过滤</em>
                <a class="all selected" href="#">所有</a>
                <a class="video" href="#">视频</a>
                <a class="photography" href="#">图片</a>
                <a class="text" href="#">文章</a>
                <a class="audio" href="#">音频</a>
            </div>
            <div class="postExcerpts">
               <div class="postExcerpt video">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Video</em>
                        </div>
                        <a class="playVideo" href="javascript:void(0);" onclick="javascript: myLightWindow.activateWindow({href: 'pages/my/media/galapagos.mp4', title: 'Waiting for the show to start in Las Vegas', author: 'Jazzmatt', caption: 'Mmmmmm Margaritas! And yes, this is me...'});">Play</a>
                    </div>
               </div>
               <div class="postExcerpt photography">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Photography</em>
                        </div>
                        <div class="imgWrap">
                            <img src="pages/my/media/78310a55b319ebc4dc7ecc118026cffc1e171637.jpg" alt="Tulips" width="1280" height="768">
                        </div>
                        <a class="view" href="javascript:void(0);" onclick="javascript: myLightWindow.activateWindow({href: 'pages/my/media/78310a55b319ebc4dc7ecc118026cffc1e171637.jpg', title: 'Waiting for the show to start in Las Vegas', author: 'Jazzmatt', caption: 'Mmmmmm Margaritas! And yes, this is me...'});">View</a>
                    </div>
               </div>
               <div class="postExcerpt text">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Text</em>
                        </div>
                        <p>Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                        <a class="read" href="javascript:void(0);" onclick="javascript: myLightWindow.activateWindow({href: 'pages/my/blank.html', params:'lightwindow_type=external'});">Read</a>            
                    </div>
               </div>
               <div class="postExcerpt audio">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit adipiscing elit adipiscing elit adipiscing elit</h2>
                            <em class="cat">Audio</em>
                        </div>
                        <div class="audioPlayer">
                            <audio>
                                <source src="pages/my/media/Maid.mp3" type="audio/mpeg">
                                <source src="pages/my/media/Maid.ogg" type="audio/ogg">
                            </audio>
                            <a class="playAudio" href="#">Play</a>
                            <div class="progressBarWrap">
                                <div class="progressBar"></div>
                            </div>
                            <a class="mute" href="#">Mute</a>
                            <div class="soundBarWrap">
                                <div class="soundBar"></div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="postExcerpt video">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Video</em>
                        </div>
                        <a class="playVideo" href="#">Play</a>
                    </div>
               </div>
               <div class="postExcerpt photography">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Photography</em>
                        </div>
                        <div class="imgWrap">
                            <img src="pages/my/media/Tulips.jpg" alt="Tulips" width="1024" height="768">
                        </div>
                        <a class="view" href="#">View</a>
                    </div>
               </div>
               <div class="postExcerpt text">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Text</em>
                        </div>
                        <p>Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                        <a class="read" href="#">Read</a>               
                    </div>
               </div>
               <div class="postExcerpt audio">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit adipiscing elit adipiscing elit adipiscing elit</h2>
                            <em class="cat">Audio</em>
                        </div>
                        <div class="audioPlayer">
                            <audio>
                                <source src="pages/my/media/Maid.mp3" type="audio/mpeg">
                                <source src="pages/my/media/Maid.ogg" type="audio/ogg">
                            </audio>
                            <a class="playAudio" href="#">Play</a>
                            <div class="progressBarWrap">
                                <div class="progressBar"></div>
                            </div>
                            <a class="mute" href="#">Mute</a>
                            <div class="soundBarWrap">
                                <div class="soundBar"></div>
                            </div>
                        </div>
                    </div>
               </div>
               <div class="postExcerpt video">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Video</em>
                        </div>
                        <a class="playVideo" href="#">Play</a>
                    </div>
               </div>
               <div class="postExcerpt photography">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Photography</em>
                        </div>
                        <div class="imgWrap">
                            <img src="pages/my/media/Tulips.jpg" alt="Tulips" width="1024" height="768">
                        </div>
                        <a class="view" href="#">View</a>
                    </div>
               </div>
               <div class="postExcerpt text">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</h2>
                            <em class="cat">Text</em>
                        </div>
                        <p>Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit. Lorem ipsum dolor sit amet, sed am nonummy nibh euismod tincidunut laoreet dolore magna alim erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                        <a class="read" href="#">Read</a>               
                    </div>
               </div>
               <div class="postExcerpt audio">
                    <div class="postExcerptInner">
                        <div class="titleAndCat">
                            <h2>Consectetuer adipiscing elit adipiscing elit adipiscing elit adipiscing elit</h2>
                            <em class="cat">Audio</em>
                        </div>
                        <div class="audioPlayer">
                            <audio>
                                <source src="pages/my/media/Maid.mp3" type="audio/mpeg">
                                <source src="pages/my/media/Maid.ogg" type="audio/ogg">
                            </audio>
                            <a class="playAudio" href="#">Play</a>
                            <div class="progressBarWrap">
                                <div class="progressBar"></div>
                            </div>
                            <a class="mute" href="#">Mute</a>
                            <div class="soundBarWrap">
                                <div class="soundBar"></div>
                            </div>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
    <footer>
        <div class="footerInner" style="float:left">
            <p class="copy">Copyright CHEN &copy; 2014. All Rights Reserved. </p>
        </div>
        <div class="footerInner" style="float:right;text-decoration: underline;">
            <a href="http://my.oschina.net/eternal/blog?catalog=479505" target="_blank">我的Blog</a><span style="padding-left:20px"></span><a href="http://git.oschina.net/eternal_rider/chen" target="_blank">项目源代码</a>
            </ul>
        </div>
    </footer>
<script src="resources/box/js/sort.js"></script>
<!--[if lte IE 8]><script src="resources/box/js/ie8AndBelow.js"></script><![endif]-->
</body>
</html>