<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://my.oschina.net/eternal/chen" %>
<!DOCTYPE html>
<html>
<head>
<base href="${c:base()}">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>${c:get("AppName")}</title>
  <link rel="stylesheet" href="resources/login/css/style.css">
  <!--[if lt IE 9]><script src="resources/login/html5.js"></script><![endif]-->
  <script type="text/javascript" src="resources/jquery.min.js"></script>
  <script type="text/javascript" src="resources/store.js"></script>
	<script type="text/javascript">
		$(function() {
				var account = store.get('account');
				if(account){
					$('#account').val(account);
				}
		});		
		function toLogin() {
			if ($('#account').val().length > 0 && $('#password').val().length > 0) {
				$('#msgDiv').html('请稍候，正在登录。。。');
				store.set('account',$("#account").val());
				$('#loginform').submit();
			} else {
				$('#msgDiv').html('错误！用户名和密码不能为空！');
			}
		}
	</script>
</head>
<body>
  <form id="loginform" method="post" action="ty/home/index/" class="login">
    <p>
      <label for="login">帐号：</label>
      <input type="text" name="account" id="account" placeholder="username">
    </p>

    <p>
      <label for="password">密码：</label>
      <input type="password" name="password" id="password" placeholder="password">
    </p>

    <p class="login-submit">
      <button type="button" onClick="toLogin();" class="login-button">登录</button>
    </p>

    <p class="forgot-password"><a href="javascript:void(0);">帐号：admin/bohecha</a></p>
    <p id="msgDiv" class="forgot-password">${requestScope.TipMsg}</p>
  </form>
</body>
</html>
